#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

#define MAX_LINE_LENGTH 1024
#define MaxPerms (S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH)

FILE *output;
char izolationPath[1024];

// Function to verify if a given path is a directory
int verifyDirectory(const char *nume);

// Recursive function to traverse directories and perform operations
void directory(const char *nume, int depth, const char *output_directory, int *Corruptnr);

// Function to print file version information
void printVersion(int fd, struct stat buffer);

// Function to compare two snapshot files
void comparesnapshot(const char *old_snapshot_path, const char *new_snapshot_path);

// Function to verify if file permissions are safe
int verifyPermissions(struct stat buffer);

int main(int argc, char *argv[]) {
    char globalDirectorypath[1024];
    if (argc < 6) {
        fprintf(stderr, "Usage: %s -o <outputdirectory> -s <izolated_directory> <directories>\n", argv[0]); // Check if enough arguments are provided.
        return EXIT_FAILURE;
    }

    int cnt = 1;
    while (argv[cnt] != NULL)
    {
        cnt++;
        if(cnt > 15){
            printf("MAX 10 Directories!\n"); // Inform the user if too many arguments are provided.
            break;
        }
    }
    
    if(strcmp(argv[1],"-o") != 0 || strcmp(argv[3],"-s") != 0)
    {
        fprintf(stderr, "Usage: %s -o <outputdirectory> -s <izolated_directory> <directories>\n", argv[0]); // Check if arguments are in the correct order.
        return EXIT_FAILURE;
    }
    else
    {
        strcpy(globalDirectorypath, argv[2]);
    }

    char output_directory[1024];
    strcpy(output_directory, argv[2]);

    strcpy(izolationPath, argv[4]);

    pid_t pid = fork();
    int cnt_wait = 0;

    if(pid < 0){
        perror("Error creating child process");
        exit(EXIT_FAILURE);
    }

    if(pid == 0){
        int Corruptnr = 0;
        for(int i=5; i<cnt; i++){
            cnt_wait++;
            if(verifyDirectory(argv[i])){
                directory(argv[i], 0, output_directory, &Corruptnr);
            }
            else
                continue;
        }
        exit(Corruptnr);
    }
    printf("\n");

    for(int i = 0; i<cnt-5; i++){
        int status;
        pid_t pid = wait(&status);
        if(WIFEXITED(status)){
            printf("IN PARENT: Child process %d terminated with pid %d and exit code %d (nr of corrupt files)\n", i+1, pid, WEXITSTATUS(status));
        }
        else
        {
            perror("Child error in main\n");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}

// Function to verify if a given path is a directory
int verifyDirectory(const char *nume){
    struct stat path;
    stat(nume, &path);
    return S_ISDIR(path.st_mode); 
}

// Recursive function to traverse directories and perform operations
void directory(const char *nume, int depth, const char *output_directory, int *Corruptnr) {
    DIR *dir;
    struct dirent *entry;
    struct stat file_info;
    char file_path[1000];

    dir = opendir(nume);
    if (dir == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        snprintf(file_path, sizeof(file_path), "%s/%s", nume, entry->d_name);
        if (stat(file_path, &file_info) == -1) {
            perror("stat");
            exit(EXIT_FAILURE);
        }

        if (S_ISDIR(file_info.st_mode)) {
            directory(file_path, depth + 1, output_directory, Corruptnr);
        }

        char path[1024] = "";
        int fd = 0;
        if(verifyPermissions(file_info) == 0)
        {
            int pid;
            int pfd[2];
            FILE *stream;

            if(pipe(pfd) < 0){
                perror("Error creating pipe\n");
                exit(EXIT_FAILURE);
            }
            if((pid = fork()) < 0)
            {
                perror("Error creating child process!\n");
                exit(EXIT_FAILURE);
            }

            if(pid == 0)
            {
                close(pfd[0]);
                dup2(pfd[1],1);

                execlp("./script.sh", "./script.sh", file_path, izolationPath, NULL);
                perror("Exec error!\n");
                exit(-99);
            }
            close(pfd[1]);

            stream = fdopen(pfd[0],"r");
            char string[1024];
            if(stream == NULL){
                perror("Read error\n");
                exit(EXIT_FAILURE);
            }
            
            fscanf(stream,"%s", string);

            close(pfd[0]);
            fclose(stream);

            int status;
            wait(&status);

            if(WIFEXITED(status) == 0){
                perror("Child error\n");
                exit(EXIT_FAILURE);
            }

            string[strcspn(string,"\n")] = '\0';
            if(strcmp(string, "Safe") !=0){
                char *filename = strrchr(string, '/');      
                filename++;     

                char IzolationLocation[2048];

                sprintf(IzolationLocation, "%s/%s", izolationPath, filename);

                if(rename(string,IzolationLocation) != 0){
                    perror("Rename error\n");
                    exit(-1);
                }

                (*Corruptnr)++;
                continue;           
            }
        }
        if(S_ISDIR(file_info.st_mode) == 0){
            if(strstr(entry->d_name, "_snapshot") == NULL){
                sprintf(path,"%s/%s_snapshot", output_directory, entry->d_name);
                if ((fd = open(path, O_WRONLY | O_APPEND | O_CREAT | O_TRUNC, MaxPerms)) == -1)//check the fd
                {
                    perror("Files could not be created\n");
                    exit(EXIT_FAILURE);
                }
                printVersion(fd,file_info);
                close(fd);
            }
            else
            {
                sprintf(path,"%s/%s_new", output_directory, entry->d_name);
                if ((fd = open(path, O_WRONLY | O_APPEND | O_CREAT | O_TRUNC, MaxPerms)) == -1)
                {
                    perror("Files could not be created (new snapshot)\n");
                    exit(EXIT_FAILURE);
                }
                printVersion(fd,file_info);
                close(fd);
                comparesnapshot(file_path, path);
            }
        }
    }

    closedir(dir);
}

// Function to print file version information
void printVersion(int fd, struct stat buffer)
{
    char st_dev[64];
    sprintf(st_dev, "%ld", buffer.st_dev);
    char st_ino[64];
    sprintf(st_ino, "%ld", buffer.st_ino);
    char st_mode[64];
    sprintf(st_mode, "%d", buffer.st_mode);
    char st_nlink[64];
    sprintf(st_nlink, "%ld", buffer.st_nlink);
    char st_uid[64];
    sprintf(st_uid, "%d", buffer.st_uid);
    char st_gid[64];
    sprintf(st_gid, "%d", buffer.st_gid);
    char st_size[64];
    sprintf(st_size, "%ld", buffer.st_size);

    char data[1024];


    sprintf(data, "ID: %s\nI-NODE NUMBER: %s\nFile TYPE : %s\nNumber of HARDLINKS: %s\nID OWNER: %s\nID GROUP: %s\nSIZE: %s\n\n", st_dev, st_ino, st_mode, st_nlink, st_uid, st_gid, st_size);



    if (write(fd, data, strlen(data)) == -1)
    {
        perror("Could not write!\n");
        exit(-2);
    }
}


// Function to compare two snapshot files
void comparesnapshot(const char *old_snapshot_path, const char *new_snapshot_path) {
    FILE *old_snapshot = fopen(old_snapshot_path, "r");
    FILE *new_snapshot = fopen(new_snapshot_path, "r");

    if (old_snapshot == NULL) {
        perror("Error opening old snapshot file");
        printf("Path: %s\n", old_snapshot_path);
        exit(EXIT_FAILURE);
    }
    if (new_snapshot == NULL) {
        perror("Error opening new snapshot file");
        printf("Path: %s\n", new_snapshot_path);
        exit(EXIT_FAILURE);
    }

    char old_line[MAX_LINE_LENGTH];
    char new_line[MAX_LINE_LENGTH];
    
    while (fgets(old_line, sizeof(old_line), old_snapshot) != NULL && fgets(new_line, sizeof(new_line), new_snapshot) != NULL) {
        if (strcmp(old_line, new_line) != 0) {
            fclose(old_snapshot);
            fclose(new_snapshot);

            if (rename(new_snapshot_path, old_snapshot_path) != 0) {
                perror("Error replacing snapshot");
                exit(EXIT_FAILURE);
            }
            return;
        }
    }

    // If files have different number of lines
    if (fgets(old_line, sizeof(old_line), old_snapshot) != NULL || fgets(new_line, sizeof(new_line), new_snapshot) != NULL) {
        fclose(old_snapshot);
        fclose(new_snapshot);

        if (rename(new_snapshot_path, old_snapshot_path) != 0) {
            perror("Error replacing snapshot");
            exit(EXIT_FAILURE);
        }
        return;
    }
    
    fclose(old_snapshot);
    fclose(new_snapshot);
}

// Function to verify if file permissions are safe
int verifyPermissions(struct stat buffer)
{
    if(buffer.st_mode & S_IRUSR)
        return 1;
    if(buffer.st_mode & S_IWUSR)
        return 1;
    if(buffer.st_mode & S_IXUSR)
        return 1;
    if(buffer.st_mode & S_IRGRP)
        return 1;
    if(buffer.st_mode & S_IWGRP)
        return 1;
    if(buffer.st_mode & S_IXGRP)
        return 1;
    if(buffer.st_mode & S_IROTH)
        return 1;
    if(buffer.st_mode & S_IWOTH)
        return 1;
    if(buffer.st_mode & S_IXOTH)
        return 1;

    return 0;
}
